﻿using System;
using System.Windows;

namespace AnimationsLessonWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ProgressBarAnimationCompleted(object sender, EventArgs e)
        {
            MessageBox.Show("Nya!");
        }
    }
}
